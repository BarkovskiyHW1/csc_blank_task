Instruction how to build project:

# clone repo and download googletests
git clone https://gitlab.com/BarkovskiyHW1/csc_blank_task.git
cd csc_blank_task
git submodule update --init --recursive

# make build directory
mkdir build
cd build

# generate projects
cmake ..

# build exe and unittests
cmake --build .

Instruction how to run:

./hash "algorithm" "path to file"

You can use algorithms: 'crc32' and 'sum32' 

Instruction how to run unittests:

./hash_unittest