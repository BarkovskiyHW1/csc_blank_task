#include "algorithm.h"

uint32_t SUM32 (std::vector<uint32_t> &v) {
    uint32_t sum_hash = 0;
    for (int i = 0; i < v.size(); ++i) {
        sum_hash += v[i];
    }
    return sum_hash;
}
    
uint32_t CRC32 (std::vector<uint32_t> &v) {
    uint32_t crc_table[256];
    uint32_t crc;

    for (int i = 0; i < 256; i++) {
        crc = i;
        for (int j = 0; j < 8; j++) {
            crc = crc & 1 ? (crc >> 1) ^ 0xEDB88320UL : crc >> 1;
            crc_table[i] = crc;
        }
    }
    crc = 0xFFFFFFFFUL;
    for (int i = 0; i < v.size() ; ++i) {
        crc = crc_table[(crc ^ v[i]) & 0xFF] ^ (crc >> 8);
    }
    return crc ^ 0xFFFFFFFFUL;
}
