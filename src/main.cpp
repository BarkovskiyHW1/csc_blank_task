#include <iostream>
#include <cstdint>

#include "readfile.h"
#include "algorithm.h"

int main(int argc, char *argv[]) {
    if (argc < 3) {
        std::cout << "Please, enter type of hash and path to file. For example 'sum32' or 'crc32' and path" << std::endl;
        return 1;
    }
    bool ok = false;
    std::vector<uint32_t> input = ReadFile(argv[2], ok);
    if (ok == false) {
        std::cout << "can not read the file" << std::endl;
        return 1;
    }

    uint32_t sum = 0;
    std::string name(argv[1]);
    if (name == "crc32") {
        sum = CRC32(input);
    } else if (name == "sum32") {
        sum = SUM32(input);
    } else {
        std::cout << "Unknown algorithm" << std::endl;
        return 1;
    }
    std::cout << sum << std::endl;
    return 0;
}
