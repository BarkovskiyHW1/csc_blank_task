#include "readfile.h"

std::vector<uint32_t> ReadFile (const std::string& path, bool &ok) {
    std::ifstream file(path, std::ios::binary | std::ios::ate);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<char> buffer(size);
    if (!file.read(buffer.data(), size)) {
        ok = false;
        return std::vector<uint32_t>();
    }
    while (buffer.size() % 4) {
        buffer.push_back('\0');
    }

    std::vector<uint32_t> vec(buffer.size()/4);
    int vsize = vec.size();
    for (int i = 0; i < vsize; ++i) {
        vec[i] = 0;
        for (int j = 0; j < 4; ++j) {
            vec[i] = (vec[i] << 8) | buffer[4*i + j];
        }
    }

    ok = true;
    return vec;
}