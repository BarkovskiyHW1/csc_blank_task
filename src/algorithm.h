#pragma once

#include <cstdint>
#include <vector>


uint32_t SUM32 (std::vector<uint32_t> &v);
uint32_t CRC32 (std::vector<uint32_t> &v);
