#pragma once

#include <fstream>
#include <vector>
#include <cstdint>
#include <iostream>

std::vector<uint32_t> ReadFile (const std::string &path, bool &ok);