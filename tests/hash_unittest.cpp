#include "gtest/gtest.h"

#include "algorithm.h"

TEST(SUM32, value)
{
	std::vector<uint32_t> vec{54, 32, 98, 33};
	uint32_t res = SUM32(vec);
	EXPECT_EQ(static_cast<uint32_t>(217), res);
}

TEST(SUM32, null)
{
	std::vector<uint32_t> null;
	uint32_t res = SUM32(null);
	EXPECT_EQ(static_cast<uint32_t>(0), res);
}

TEST(CRC32, value)
{
	std::vector<uint32_t> vec{111};
	uint32_t res = CRC32(vec);
	EXPECT_EQ(static_cast<uint32_t>(0x0F0F9344), res);
}

TEST(CRC32, null)
{
	std::vector<uint32_t> null;
	uint32_t res = CRC32(null);
	EXPECT_EQ(static_cast<uint32_t>(0), res);
}

